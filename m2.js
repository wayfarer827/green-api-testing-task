const amqp = require('amqplib');

const connectAndConsume = async () => {
  try {
    const connection = await amqp.connect('amqp://localhost');
    const channel = await connection.createChannel();

    await channel.assertQueue('m2_queue', { durable: true });
    channel.prefetch(1);

    console.log('M2 | Ожидание заданий из очереди RabbitMQ...');

    channel.consume('m2_queue', async (message) => {
      const number = parseInt(message.content.toString());
      console.log(`M2 | Из очереди пришло число: ${number}`)
      try {
        const result = number * 2;
        console.log(`M2 | В результате манипуляций было получено новое число: ${result}`)
        channel.sendToQueue(message.properties.replyTo, Buffer.from(result.toString()), {
          correlationId: message.properties.correlationId
        });
        channel.ack(message);
        console.log(`M2 | Новое число отправлено на M1`)
      } catch (error) {
        console.error(error);
        channel.reject(message, false);
      }
    });
  } catch (error) {
    console.error(error);
  }
};

connectAndConsume();
