const http = require('http');
const amqp = require('amqplib');

const port = 3000

const server = http.createServer(async (req, res) => {
  if (req.method === 'GET' && req.url.startsWith('/?number=')) {
    try {
      const number = parseInt(req.url.split('=')[1]);

      if (isNaN(number)) {
        throw new Error('Некорректное значение числа');
      }

      console.log(`M1 | Из параметров запроса было получено число: ${number}`)

      const connection = await amqp.connect('amqp://localhost');
      const channel = await connection.createChannel();
      console.log(`M1 | Подключение к AMQP прошло успешно`)

      const responseQueue = await channel.assertQueue('', { exclusive: true });
      const correlationId = generateUuid();

      channel.consume(responseQueue.queue, (message) => {
        if (message.properties.correlationId === correlationId) {
          const result = parseInt(message.content.toString());
          console.log(`M1 | Из очереди пришло число ${result}`)
          res.statusCode = 200;
          res.setHeader('Content-Type', 'text/plain');
          res.end(`Результат: ${result}`);
          console.log(`M1 | Ответ был отправлен на клиент ${result}`)
          channel.close();
          connection.close();
        }
      }, { noAck: true });

      const isSent = channel.sendToQueue('m2_queue', Buffer.from(number.toString()), {
        correlationId,
        replyTo: responseQueue.queue
      });

      if (isSent) {
        console.log('M1 | Сообщение успешно отправлено в очередь');
      } else {
        throw new Error('M1 | Ошибка при отправке сообщения в очередь');
      }


    } catch (error) {
      console.error(error);
      res.statusCode = 500;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Произошла ошибка');
    }
  } else {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Страница не найдена');
  }
});

server.listen(port, () => {
  console.log(`Сервер М1 запущен на порту ${port}`);
});

function generateUuid() {
  return Math.random().toString() + Math.random().toString() + Math.random().toString();
}

